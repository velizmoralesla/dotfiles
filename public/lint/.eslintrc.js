
module.exports = {

    'env': {
        'es6': true,
        'broser': true
    },

    'extends': [
        'prettier',
        'prettier/react'
    ],

    'plugins': [
        'prettier'
    ],

    'rules': {

        'block-scoped-var': 'warn',

        'comma-dangle': [
            'error', {
                'arrays': 'never',
                'exports': 'never',
                'functions': 'never',
                'imports': 'never',
                'objects': 'never'
            }
        ],

        'eqeqeq': 'error',
        'indent': ['warn', 4],
        'linebreak-style': ['error', 'unix'],
        'no-cond-assign': ['error', 'except-parens'],
        'no-console': ['warn', { 'allow': ['error', 'info', 'warn'] }],
        'no-constant-condition': 'error',
        'no-control-regex': 'error',
        'no-debugger': 'error',
        'no-dupe-args': 'error',
        'no-dupe-keys': 'error',
        'no-duplicate-case': 'error',
        'no-empty-character-class': 'error',
        'no-empty': ['warn', { 'allowEmptyCatch': false }],
        'no-ex-assign': 'error',
        'no-extra-semi': 'warn',
        'no-func-assign': 'error',
        'no-inner-declarations': ['warn', 'both'],
        'no-irregular-whitespace': 'warn',
        'no-negated-in-lhs': 'error',
        'no-obj-calls': 'error',
        'no-sparse-arrays': 'error',
        'no-unreachable': 'error',

        'prettier/prettier': [
            'error', {
                'singleQuote': true
            }
        ],

        'quote-props': ['warn', 'as-needed'],
        'quotes': ['warn', 'single'],

        'valid-jsdoc': [
            'warn', {
                'prefer': {
                    'arg': 'param',
                    'argument': 'param',
                    'return': 'returns',
                    'virtual': 'abstract'
                }
            }
        ]


    }

};