
# If not running interactively, dont' do anything
[[ $- != *i* ]] && return   

# Append to the Bash hisotry file, rather than overwriting it
shopt -q -s histappend

# Autocorrect typos when using 'cd'
shopt -q -s cdspell

# Make sure display get updated when terminal window get resized
shopt -q -s checkwinsize

# Case-insensitive globbing (used in pathname expansion) 
shopt -q -s nocaseglob

# Enable variables on cd command
shopt -q -s cdable_vars

# Enable bash completion {{{

if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi


alias ll="ls -l"
alias la="ls -Al"
alias ls="ls --color"
alias lss="ls -AhoS"

alias ..="cd .."
alias cdd="cd ~/Documentos"

alias grep="grep --color"
alias egrep="egrep --color"
alias fgrep="fgrep --color"


export EDITOR=vim
export VISUAL=vim
export PAGER=less
export BROWSER=chromium
export HISTSIZE=50
export HISTFILESIZE=$HISTSIZE

# Variables for cdable_vars
export dotfiles="/home/$(whoami)/origin/dotfiles/"
export gists="/home/$(whoami)/origin/gists/"

# Avoid duplicates and commands that begins with a space
export HISTCONTROL=ignoreboth

export TERM='xterm-256color'

export PS1='\u@\h \W$(getBranch)\$ '

getBranch() {
    if  [ $(git rev-parse --is-inside-work-tree &> /dev/null; echo "${?}") == '0' ]; then
        git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
    fi
}
