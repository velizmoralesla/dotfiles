
" TABLA DE CONTENIDOS
"
" 01 GUI
" 02 TABULACIONES Y LINEAS
" 03 BUSQUEDA
" 04 FUNCIONALIDADES
" 05 ARCHIVOS Y DIRECTORIOS
"   autocmd
"   netrw
" 06 KEYMAPS
"   insert mode
"   normal mode
" 07 CONFIGURACIONES ESPECIALES



" = 01 UI =
syntax on                       " Muestra colores según sintaxis.
set t_Co=256                    " Permite compatibilidad para 256 colores.
set number relativenumber       " Agrega números de línea relativos y absolutos.
set numberwidth=3               " Establece el ancho de los números de línea.
set laststatus=2                " Muestra siempre el 'status line'.
set ruler                       " Muestra los números de línea y columna actuales.
set cmdheight=1                 " Establece la altura de la línea de comandos.
set wildmenu                    " Mejora la interfaz de autocompletado de la línea de comandos.
set showcmd                     " Muestra los comandos mientras se introducen.
set scrolloff=10                " Líneas debajo y sobre el cursor mientras este se desplaza.
set noshowmatch                 " Evita que el cursos salte entre las llaves insertadas.


" = 02 TABULACIONES Y LINEAS =
set nowrap                      " Evita que las líneas que superan el largo de la ventana agreguen un 'salto de línea visual'.
set expandtab                   " Convierte las tabulaciones en espacios.
set autoindent                  " Copia el indentado de la línea actual en la siguiente.
set tabstop=4                   " Establece el tamaño de las tabulaciones.
set shiftwidth=4                " Número de espacios para 'autoindent'.


" = 03 BUSQUEDA =
set wrapscan                    " Permite ciclar las búsquedas dentro del archivo.
set path+=**                    " Incluye archivos dentro de subcarpetas del 'pwd'.
set ignorecase                  " Ignora el 'case' en búsquedas.
set incsearch                   " Muestra los resultados de la búsqueda mientras se escribe.


" = 04 FUNCIONALIDADES =
set nocompatible                " Evita conflictos con versiones anteriores del editor.
set lazyredraw                  " Evita que la pantalla muestre los cambios mientras se ejecuta un macro, registros y otros comandos.
set history=50                  " Limita el historial de comandos.
set mouse+=a                    " Permite el uso del mouse en los modos seleccionados.
set clipboard=unnamedplus       " Permite el uso de portapapeles.
set timeout timeoutlen=1000     " Tiempo de espera para ejecución de un atajo de teclas.
set ttimeout ttimeoutlen=100    " Tiempo de espera para ejecución de un atajo de teclas.


" = 05 ARCHIVOS Y DIRECTORIOS =

set nobackup noswapfile         " Evita crear archivos temporales o de respaldo.
set nomodeline modelines=0      " Evita el uso de modelines.

" autocmd
autocmd FileType gitcommit set colorcolumn=50,72
autocmd FileType tex set colorcolumn=100

" netrw
let g:netrw_banner=0            " Remover el banner que ocupa la mitad de la pantalla.


" = 06 KEYMAPS =

let mapleader="\<Space>"                           " Tecla lider.
imap jk <esc>                                      " Tecla de escape.
imap JK <esc>                                      " Tecla de escape.
nn <F2> :e $MYVIMRC<CR>                            " Carga el archivo .vimrc para modificaciones.
nn <F3> :let &cc = &cc == '' ? '100' : ''<CR>      " Alterna líneas verticales.
nn <F5> :source $MYVIMRC<CR>                       " Recarga el archivo .vimrc.
nn <F6> :set rnu! <CR>                             " Alterna los números de línea relativos.
nn <Leader>l :bn<CR>                               " Abre el siguiente buffer.
nn <Leader>h :bp<CR>                               " Abre el buffer anterior.
nn <Leader>v :vsplit<CR>                           " Dividir la ventana verticalmente.
nn <Leader>o :split<CR>                            " Dividir la ventana horizontalmente.
nn <C-h> <C-w>h                                    " Desplazarce una ventana a la derecha.
nn <C-l> <C-w>l                                    " Desplazarce una ventana a la izquierda.
nn <C-j> <C-w>j                                    " Desplazarce una ventana hacia abajo.
nn <C-k> <C-w>k                                    " Desplazarce una ventana hacia arriba.
nn <Leader>ss :setlocal spell spelllang=es_es<CR>  " Ortografía en español.
nn <Leader>se :setlocal spell spelllang=en_us<CR>  " Ortografía en inglés.


" = 07 CONFIGURACIONES ESPECIALES =

" Establece el color de las lineas verticales definidas en 'colorcolumn'.
highlight ColorColumn ctermbg=lightgreen



