
# TABLE OF CONTENTS / TABLA DE CONTENIDOS

- [DOTFILES / ARCHIVOS DE CONFIGURACION](#dotfiles--archivos-de-configuracion)

- [INSTALLATION / INSTALACION](#installation--instalacion)

## DOTFILES / ARCHIVOS DE CONFIGURACION

- These are the dotfiles that I use on my computers.
- Estos son los archivos de configuración que utilizo en mis equipos.

## INSTALLATION / INSTALACION

- To complete the installation the Ansible package is required.
- Para la instalación es necesario el paquete Ansible.

1. ` git clone` https://gitlab.com/velizmoralesla/dotfiles.git
2. ` cd dotfiles/`
3. ` ansible-playbook install.yml`
